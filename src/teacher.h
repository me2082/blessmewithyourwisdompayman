#pragma once
#include "person.h"
#include "schoolIDSystem.h"

class Teacher : public Person, public ID
{
    private:
        int salary;
    public:
        int getSalary();
        void setSalary(int salary);
};