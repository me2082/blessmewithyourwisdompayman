#include "courses.h"
#include "teacher.h"

string Courses::getName()
{
    return courseName;
}

void Courses::setName(string courseName)
{
    this->courseName = courseName;
}

string Courses::getTopic()
{
    return courseTopic;
}

void Courses::setTopic(string courseTopic)
{
    this->courseTopic = courseTopic;
}

void assignTeacher(Teacher test)
{
    this->courseTeachers.push_back(test)
}
