#include <cassert>
#include <iostream>
#include <fstream>
#include "hps/src/hps.h"
#include "writer.h"

int writeFile() 
{
  std::pair<std::string, int> pair1 = {"Craig Marais", 37};
  std::pair<std::string, int> pair2 = {"JC Bailey", 30};

  std::vector<std::pair<std::string, int>> data = {pair1, pair2};

  std::string serialized = hps::to_string(data);
  std::cout << "Writing " << serialized.size() << " Bytes" << std::endl;

  std::ofstream dataFile;
  dataFile.open("data.txt", std::ios::binary | std::ios::app);
  dataFile << serialized <<std::endl;
  dataFile.close();
  return 0;
}