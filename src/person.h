#pragma once
using namespace std;
#include <iostream>

class Person
{
protected:
    string firstName, lastName;

public:
    void setName(string firstName, string lastName);
    string getName();
};